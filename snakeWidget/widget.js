
var panels = {
	"dim": "y Axis",
	"breakby": "Split By",
	"date": "Time (sec)",
	"metric":"Sort/Color"
}

prism.registerWidget("snakewidget", {

	name : "snakewidget",
	family : "Line",
	title : "Snake Chart",
	hideNoResults: true,
	iconSmall : "/plugins/snakewidget/widget-24.png",
	styleEditorTemplate: "/plugins/snakewidget/styler.html",
	style: {
		opacity: 0.7,
		yAxisLabelEnabled: true,
		xAxisLabelEnabled: false,
	},
	data : {
		selection : [],
		defaultQueryResult : {},	
		panels : [
			{
				name: panels.date,
				type: 'visible',
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: panels.breakby,
				type: 'visible',
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: panels.dim,
				type: 'visible',
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: panels.metric,
				type: 'visible',
				itemAttributes: ["color","sort"],
				allowedColoringTypes: function() {
					return {
						color: true,
						condition: false,
						range: false
					}
				},
				metadata: {
					types: ['measures'],
					maxitems: 1
				},
				itemAdded: function(widget, item) {
					var colorFormatType = $$get(item, "format.color.type");
					if ("color" === colorFormatType || "color" === colorFormatType) {
						var color = item.format.color.color;
						defined(color) && "transparent" != color && "white" != color && "#fff" != color && "#ffffff" != color || $jaql.resetColor(item)
					}
					"range" === colorFormatType && $jaql.resetColor(item), defined(item, "format.color_bkp") && $jaql.resetColor(item), defined(item, "format.members") && delete item.format.members
				}
			},
			{
				name: 'filters',
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],
		
		canColor: function (widget, panel, item) {

            return (panel.name === panel.metric );
        },
		
		allocatePanel: function (widget, metadataItem) {
			
			// measure
			if (prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel(panel.metric).items.length === 0) {

				return panel.metric;
			}
			// dimension
			else if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel(panel.dim).items.length === 0) {

				return panel.dim;
			}
		},

		// returns true/ reason why the given item configuration is not/supported by the widget
		isSupported: function (items) {

			return this.rankMetadata(items, null, null) > -1;
		},

		// ranks the compatibility of the given metadata items with the widget
		rankMetadata: function (items, type, subtype) {

			var a = prism.$jaql.analyze(items);

			// require 1 measure, 2 dimension
			if (a.measures.length >= 0 && a.dimensions.length >= 2) {

				return 0;
			}

			return -1;
		},

		// populates the metadata items to the widget
		populateMetadata: function (widget, items) {

			var a = prism.$jaql.analyze(items);

			// allocating dimensions
			if (a.dimensions.length > 0) {
				widget.metadata.panel(panels.date).push(a.dimensions[0]);
			}
			if (a.dimensions.length > 1) {
				widget.metadata.panel(panels.dim).push(a.dimensions[1]);
			}
			if (a.measures.length > 0) {
				widget.metadata.panel(panels.metric).push(a.measures[0]);
			}
			
			// allocating filters
			widget.metadata.panel("filters").push(a.filters);
		},

		// builds a jaql query from the given widget
		buildQuery: function (widget) {

			//	Reset the query complete counter
			var query = { 
				datasource: widget.datasource, 
				metadata: [] 
			}

			//	Make sure we have all required items
			var isValid = (widget.metadata.panel(panels.date).items.length == 1)
							&& (widget.metadata.panel(panels.dim).items.length == 1)
							&& (widget.metadata.panel(panels.breakby).items.length == 1);
			if (isValid) {

				//	Add the date dimension (x-axis)
				widget.metadata.panel(panels.date).items.forEach(function(item){
					query.metadata.push(item);
				})

				//	Add the dimension to use for y-axis
				widget.metadata.panel(panels.dim).items.forEach(function(item){
					query.metadata.push(item);
				})

				//	Add the break by dimension
				widget.metadata.panel(panels.breakby).items.forEach(function(item){
					query.metadata.push(item);
				})

				//	Add the metric by dimension
				widget.metadata.panel(panels.metric).items.forEach(function(item){
					query.metadata.push(item);
				})

				//	Add any widget filters
				widget.metadata.panel("filters").items.forEach(function(item){
					item = $$.object.clone(item, true);
					item.panel = "scope";
					query.metadata.push(item);
				})
			}
				
            //	Return the widget's query
            return query;
		},

		//create highcharts data structure
		processResult : function (widget, queryResult) {

			//	Create a mapping of products and stations
			var breakBys = {};
			var yAxisItems = [];

			//	Get the metadata labels
			var dateLabel = $$get(widget.metadata.panel(panels.date).items, '0.jaql.title', 'Date'),
				yAxisLabel = $$get(widget.metadata.panel(panels.dim).items, '0.jaql.title', 'Dimension'),
				breakByLabel = $$get(widget.metadata.panel(panels.breakby).items, '0.jaql.title', 'Split By');

			//	Loop through each row of data
			queryResult.$$rows.forEach(function(row){

				//	Get the values
				var timestamp = row[0].data,
					yAxisItem = row[1].text,
					breakByItem = row[2].text;

				//	Make sure the yAxisItem has been tracked
				var yIndex = yAxisItems.indexOf(yAxisItem);
				if (yIndex == -1) {
					yIndex = yAxisItems.length;
					yAxisItems[yIndex] = yAxisItem;
				}

				//	Create the data point
				var point = {
					x: timestamp,
					xDimension: dateLabel,
					y: yIndex,
					yDimension: yAxisLabel,
					name: breakByItem
				}

				//	Save this data point
				if (breakBys[breakByItem]) {
					//	Already have an object for this product, add to the data array
					breakBys[breakByItem].data.push(point);
				} else {
					//	New product, create the series object 
					breakBys[breakByItem] = {
						name: breakByItem,
						dimension: breakByLabel,
						type: 'line',
						data: [point]
					}
				}
			})

			//	Build a query result object to send back to widget
			var result = {
				dimensions: yAxisItems,
				dataSeries: []
			}

			//	Add all series data to the query result object
			for (series in breakBys){
				if (breakBys.hasOwnProperty(series)){
					result.dataSeries.push( breakBys[series] );
				}
			}
			return result;
		}
	},

	render : function (widget, args) {

		/* 	Init 	*/

		//	Define the widget id
		var divId = "snake-chart" + widget.oid;
		
		//	Figure out the container
		var container = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('.widget-body', $('widget[widgetid="' + widget.oid + '"]'));

		//	Setup HTML elements
		container.empty();
		container.append("<div id='" + divId +  "' class='snake-chart'></div>")

		//	What color should we use
		var color = prism.$ngscope.dashboard.style.options.palette.colors[0];

		//	Time formatter
		function secondFormatter(seconds){
			//	Create a moment object from the # seconds, return the formatted string
			return moment.unix(seconds).format("h:mm:ss a");
		}

		/* Formatter Functions 	*/

		//	y axis formatter
		var yAxisFormatter = function(){
			
			//	Lookup the label for the axis
			var axisLabel = widget.queryResult.dimensions[this.value];
			return axisLabel ? axisLabel : ""
		}

		//	x axis formatter
		var xAxisFormatter = function(){
			
			//	format the time in seconds
			return secondFormatter(this.value);
		}

		//	Tooltip formatter
		var tooltipFormatter = function(){

			//	Figure out the x and y axis labels
			var yAxis = widget.queryResult.dimensions[this.y],
				xAxis = secondFormatter(this.x),
				xAxisDim = this.points[0].point.xDimension,
				yAxisDim = this.points[0].point.yDimension,
				seriesDim = this.points[0].series.userOptions.dimension;

			//	Define the header
			var tooltip = "<div class='highcharts-label highcharts-tooltip snake-tooltip-container'>"
							+ "<span class='snake-tooltip-header'>" + seriesDim + " at " + yAxisDim + " " + yAxis + " (" + xAxis + ")</span><br />"
							+ "<table><tbody>";

			//	List all the points at this intersection
			this.points.forEach(function(point){
				tooltip += "<tr><td>" + point.series.name + "</td></tr>";
			})

			//	Close the tooltip html, and return
			tooltip += "</tbody></table></span></div>";
			return tooltip;
		}


		/* 	Configure Options/Render 	*/
		
		//	Build the Highcharts Object
		var options = {
			boost: {
				useGPUTranslations: true,
				debug: {
					timeSetup: false,
					timeSeriesProcessing: false,
					timeBufferCopy: false,
					timeKDTree: false,
					showSkipSummary: false
				}
			},
			chart: {
				zoomType: 'x'
			},
			exporting: {
				enabled: false
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'linear',
				labels: {
					formatter: xAxisFormatter
				}
			},
			yAxis: {
				reversed: true,
				labels: {
					formatter: yAxisFormatter
				},
				title: {
				  text: ''
				}
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					color: color,
					className: "snake-chart-line",
					lineWidth: 1,
					marker: {
						enabled: false
					}
				}
			},
			series: widget.queryResult.dataSeries,
			tooltip: {
				formatter: tooltipFormatter,
		        shared: true
			}
		};

		//	Generate the highchart
		Highcharts.chart(divId, options)
	},

	destroy : function (widget, args) {}
});