
mod.controller('stylerController', ['$scope',
    function ($scope) {

        /**
         * variables
         */


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            $scope.model = $$get($scope, 'widget.style');
        });



        /**
         * public methods
         */

        $scope.setYAxis = function (value) {

            $scope.model.yAxisLabelEnabled = value;

            _.defer(function () {
                $scope.$root.widget.refresh();

            });
        };

        $scope.setXAxis = function (value) {

            $scope.model.xAxisLabelEnabled = value;

            _.defer(function () {
                $scope.$root.widget.refresh();

            });
        };


        $scope.refreshWidget = function () {         
            
            //  Redraw the widget
            _.defer(function () {
                $scope.$root.widget.refresh();
            });
        };  
    }
]);